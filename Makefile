install:
	go get ./...
build:
	go build crawley.go
test:
	go test ./... -cover
