package main

import (
	"fmt"
	"os"
	"sync"

	crawler "gitlab.com/girishgkoundinya/crawley/crawler"
)

const (
	depthValue = 2
)

func parseArguments() string {
	domainArg := os.Args[1:]
	if len(domainArg) == 1 {
		return domainArg[0]
	}
	return ""
}

func main() {
	domainArgument := parseArguments()

	if domainArgument == "" {
		fmt.Println("Please pass the correct arguments")
		os.Exit(1)
	}
	crawlWebsite(domainArgument)
}

func crawlWebsite(domain string) {
	var siteStore = crawler.NewSite(domain)
	var wg = new(sync.WaitGroup)
	wg.Add(1)
	go crawler.Crawl(domain, depthValue, wg, siteStore)
	wg.Wait()
	siteStore.PrettyPrint()
}
