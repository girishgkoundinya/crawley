package network

import (
	"io/ioutil"
	"net/http"
)

//FetchHTML -- Fetches HTML content
func FetchHTML(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", buildError("Error while fetching data")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", buildError("Error while parsing body")
	}
	return string(body), nil
}

//Error - error while making a network operation
type Error struct {
	errorMessage string
}

func (e *Error) Error() string {
	return e.errorMessage
}

func buildError(text string) error {
	return &Error{text}
}
