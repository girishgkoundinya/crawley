package network

import (
	"testing"

	"gotest.tools/assert"
)

func TestFetchHTML_NOURL(t *testing.T) {
	_, err := FetchHTML("sample")
	assert.Error(t, err, "Error while fetching data")
}

func TestFetchHTML_NOScheme(t *testing.T) {
	_, err := FetchHTML("httpbin.org/get")
	assert.Error(t, err, "Error while fetching data")
}

func TestFetchHTML_IncorrectScheme(t *testing.T) {
	_, err := FetchHTML("file://httpbin.org/get")
	assert.Error(t, err, "Error while fetching data")
}

func TestFetchHTML_EmptyURL(t *testing.T) {
	_, err := FetchHTML("")
	assert.Error(t, err, "Error while fetching data")
}
func TestFetchHTML(t *testing.T) {
	response, err := FetchHTML("https://httpbin.org/get")
	assert.NilError(t, err)
	assert.Assert(t, len(response) != 0) // NotEmpty
}
