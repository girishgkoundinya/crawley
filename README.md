# Crawley

Crawley is a web crawler written in Go.

## Installation

```bash
make install
make build
```

## Usage

```bash
./crawley https://golang.org

 Site Map For https://golang.org	

 https://golang.org/doc/
 https://golang.org/doc/tos.html
 https://golang.org/doc/copyright.html
 https://golang.org/
 https://golang.org/project/
 https://golang.org
 https://golang.org/dl/
 https://golang.org/pkg/
 https://golang.org/blog/
 https://golang.org/help/

 This site has 10 pages 


```
Run tests with coverage
```bash
make test


ok  	_/Users/girishk/Go/src/gitlab.com/girishgkoundinya/crawley/crawler	(cached)	coverage: 87.5% of statements
ok  	_/Users/girishk/Go/src/gitlab.com/girishgkoundinya/crawley/network	(cached)	coverage: 90.0% of statements
ok  	_/Users/girishk/Go/src/gitlab.com/girishgkoundinya/crawley/parser	(cached)	coverage: 100.0% of statements

```


## Structure

```bash
crawley
│   README.md
│   crawley.go --- Command line interface for crawley.
│
└───network
│   │   client.go --- HTTP client to make network call.
└───parser
|    │   parser.go --- HTML parser to identify links.
|
└───crawler
    │   site.go --- Model representation of a Site
    │   crawler.go --- Crawler

```

## Note

1. Uses a depth value of 2 for now, have not made it configurable.
