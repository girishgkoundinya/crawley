package parser

import (
	"testing"

	"gotest.tools/assert"
)

const (
	basicHTML string = `<!DOCTYPE html>
	<html>
	<body>
	
	<h2>The href Attribute</h2>
	<p>HTML links are defined with the a tag. The link address is specified in the href attribute:</p>
	
	<a href="https://www.w3schools.com/sample/link1">This is a link</a>
	<a href="/sample/link2">This is a link</a>
	<a href="www.facebook.com"> Facebook </a>
	</body>
	</html>`

	malFormedHTML string = `<!DOCTYPE html>
	<html>
	<body>
	
	<h2>The href Attribute</h2>
	<p>HTML links are defined with the a tag. The link address is specified in the href attribute:</p>
	
	<a hrf="https://www.w3schools.com/sample/link">This is a link</a>
	<href="/sample/link">This is a link</a>

	</body>
	</html>`
)

func TestGetLinks(t *testing.T) {
	links := GetLinks("https://www.w3schools.com", basicHTML)
	assert.Assert(t, len(links) == 2)
}

func TestGetLinks_URLWithTrailingSlash(t *testing.T) {
	links := GetLinks("https://www.w3schools.com/", basicHTML)
	assert.Assert(t, len(links) == 2)
}

func TestGetLinks_URLForAbsolutleLink(t *testing.T) {
	links := GetLinks("https://www.w3schools.com/", basicHTML)
	assert.Equal(t, links[0], "https://www.w3schools.com/sample/link1")
}

func TestGetLinks_URLForRelativeLinks(t *testing.T) {
	links := GetLinks("https://www.w3schools.com/", basicHTML)
	assert.Equal(t, links[1], "https://www.w3schools.com/sample/link2")
}

func TestGetLinks_MalformedHTML(t *testing.T) {
	links := GetLinks("https://www.w3schools.com", malFormedHTML)
	assert.Assert(t, len(links) == 0)
}
