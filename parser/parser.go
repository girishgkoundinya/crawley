package parser

import (
	"strings"

	"golang.org/x/net/html"
)

const (
	aTag    = "a"
	hrefTag = "href"
)

//GetLinks -- Returns an array links from a HTML page
func GetLinks(domain string, htmlString string) []string {
	domain = removeTrailing(domain)
	links := []string{}
	z := html.NewTokenizer(strings.NewReader(htmlString))
	for {
		tt := z.Next()
		switch {
		case tt == html.ErrorToken:
			//Identifies end of html page
			return links
		case tt == html.StartTagToken:
			t := z.Token()
			isAnchor := t.Data == aTag
			if isAnchor {
				for _, a := range t.Attr {
					if a.Key == hrefTag {
						if strings.HasPrefix(a.Val, "/") {
							links = append(links, domain+a.Val)
						} else if strings.HasPrefix(a.Val, domain) {
							links = append(links, a.Val)
						}
						break
					}
				}
			}
		}
	}
}

func removeTrailing(domain string) string {
	if strings.HasSuffix(domain, "/") {
		domain = domain[:len(domain)-len("/")]
	}
	return domain
}
