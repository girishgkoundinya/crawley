package crawler

import (
	"fmt"
	"sync"
)

//Site - struct that holds base domain and site map information
type Site struct {
	sync.Mutex
	SiteURL string
	SiteMap map[string]struct{}
}

//NewSite - Creates a new Site instance
func NewSite(domain string) *Site {
	return &Site{
		SiteMap: map[string]struct{}{},
		SiteURL: domain,
	}
}

//Visited - Checks if link has been visited
func (site *Site) Visited(link string) bool {
	site.Lock()
	defer site.Unlock()
	if _, ok := site.SiteMap[link]; ok {
		return true
	}
	return false
}

//AddToSiteMap - Adds link to site map attribute of struct
func (site *Site) AddToSiteMap(link string) {
	site.Lock()
	defer site.Unlock()
	site.SiteMap[link] = struct{}{}
}

//PrettyPrint - Helper function to print site details
func (site *Site) PrettyPrint() {
	site.Lock()
	defer site.Unlock()
	fmt.Printf("\n Site Map For %v\t\n", site.SiteURL)
	for url := range site.SiteMap {
		fmt.Printf("\n %v", url)
	}
	fmt.Printf("\n\n This site has %d pages \n", len(site.SiteMap))
}
