package crawler

import (
	"sync"
	"testing"

	"gotest.tools/assert"
)

func TestCrawler(t *testing.T) {
	domain := "http://golang.org"
	site := NewSite(domain)
	var wg = new(sync.WaitGroup)
	wg.Add(1)
	go Crawl(domain, 2, wg, site)
	wg.Wait()
	assert.Assert(t, len(site.SiteMap) == 11)
}

func TestCrawler_WithIncorrectSite(t *testing.T) {
	domain := "http://golang.org"
	site := NewSite(domain)
	var wg = new(sync.WaitGroup)
	wg.Add(1)
	go Crawl("hello", 2, wg, site)
	wg.Wait()
	assert.Assert(t, len(site.SiteMap) == 1)
}

func TestCrawler_With0DepthValue(t *testing.T) {
	domain := "http://golang.org"
	site := NewSite(domain)
	var wg = new(sync.WaitGroup)
	wg.Add(1)
	go Crawl("hello", 0, wg, site)
	wg.Wait()
	assert.Assert(t, len(site.SiteMap) == 0)
}
func TestCrawler_WithNegDepthValue(t *testing.T) {
	domain := "http://golang.org"
	site := NewSite(domain)
	var wg = new(sync.WaitGroup)
	wg.Add(1)
	go Crawl("hello", -1, wg, site)
	wg.Wait()
	assert.Assert(t, len(site.SiteMap) == 0)
}
