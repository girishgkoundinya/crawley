package crawler

import (
	"sync"

	net "gitlab.com/girishgkoundinya/crawley/network"
	HTMLParser "gitlab.com/girishgkoundinya/crawley/parser"
)

//Crawl - crawls website and links in websites.
func Crawl(url string, depth int, wg *sync.WaitGroup, site *Site) {
	defer wg.Done()
	if depth <= 0 {
		return
	}
	site.AddToSiteMap(url)
	htmlContent, err := net.FetchHTML(url)
	if err != nil {
		return
	}
	linksArray := HTMLParser.GetLinks(url, htmlContent)
	for _, link := range linksArray {
		if !site.Visited(link) {
			wg.Add(1)
			go Crawl(link, depth-1, wg, site)
		}
	}
}
